
# Set up FlowComponent
flow = new FlowComponent
flow.showNext(Home)

# Switch on click
btn_find_cover.onClick ->
	flow.showNext(Products_Compare)

btn_logo.onClick ->
	flow.showPrevious(Home)
	
btn_pulse_extras.onClick ->
	flow.showNext(Compare)
	flow.scroll.onMove ->
		# Make left/right panels sticky
		left_panel_nav_.y = flow.scroll.scrollY
		right_panel_quote_.y = flow.scroll.scrollY
	select_pulse_extras.onClick ->
		flow.showNext(Review)
		flow.scroll.onMove ->
			if flow.scroll
				left_panel_nav__1.y = flow.scroll.scrollY
				right_panel_quote__1.y = flow.scroll.scrollY
		
fixed_header.onClick ->
	flow.showNext(Home)
	
fixed_header_1.onClick ->
	flow.showNext(Home)
	
fixed_header_2.onClick ->
	flow.showNext(Home)

signup_btn.onClick ->
	flow.showNext(Signup)