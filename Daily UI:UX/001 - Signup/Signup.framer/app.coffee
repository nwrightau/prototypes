# Dynamic loader in case we need jQuery
{DynamicLoader} = require "DynamicLoader"

DynamicLoader.series(['https://code.jquery.com/jquery-3.2.1.min.js', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js']).then(->
	WebFont.load google: families: [
		'Montserrat'
	])
.catch(->
	# error
)

# Required for passcode screen
keyCount = 0

# Passcode state switch
passcodeStateSwitch = (directive) ->
	if directive == 'cycle'
		passcodeInputs[0].stateCycle "active", "inactive"
		passcodeInputs[1].stateCycle "active", "inactive"
		passcodeInputs[2].stateCycle "active", "inactive"
		passcodeInputs[3].stateCycle "active", "inactive"

	if directive == 'on'
		passcodeInputs[0].states.switch "active"
		passcodeInputs[1].states.switch "active"
		passcodeInputs[2].states.switch "active"
		passcodeInputs[3].states.switch "active"
	
	if directive == 'off'
		passcodeInputs[0].states.switch "inactive"
		passcodeInputs[1].states.switch "inactive"
		passcodeInputs[2].states.switch "inactive"
		passcodeInputs[3].states.switch "inactive"
		passcodeText[0].states.switch "inactive"
		passcodeText[1].states.switch "inactive"
		passcodeText[2].states.switch "inactive"
		passcodeText[3].states.switch "inactive"
		keyCount = 0
	

# Passcode entry function
passcodeEntry = (event) ->
	e = event
	keyValue = String.fromCharCode(e.keyCode)
	e.preventDefault() # Prevents OS X making the annoying sound

	# Delete in correct order
	if e.keyCode == 8
		if passcodeText[3].text != ""
			passcodeText[3].text = ""
			keyCount = 3
		else if passcodeText[2].text != ""
			passcodeText[2].text = ""
			keyCount = 2
		else if passcodeText[1].text != ""
			passcodeText[1].text = ""
			keyCount = 1
		else if passcodeText[0].text != ""
			passcodeText[0].text = ""
			keyCount = 0

	# Capture numbers only 
	if keyValue.match(/^[0-9]+$/) != null
		passcodeStateSwitch('on')
		keyCount++
		if keyCount == 1
			passcodeText[0].text = keyValue
			passcodeText[0].states.switch "active"
		if keyCount == 2
			passcodeText[1].text = keyValue
			passcodeText[1].states.switch "active"
		if keyCount == 3
			passcodeText[2].text = keyValue
			passcodeText[2].states.switch "active"
		if keyCount == 4
			passcodeText[3].text = keyValue
			passcodeText[3].states.switch "active"

# Proxy function for the event
inputEventProxy = (event) ->
	topInputEntry = false
	bottomInputEntry = false
	
	#print inputs[0].states.current.name
	#print inputs[1].states.current.name
	
	if inputs[0].states.current.name == "textEntry"
		topInputEntry = true
	else 
		topInputEntry = false
	
	if inputs[1].states.current.name == "textEntry" || inputs[1].states.current.name == "twoActive"
		bottomInputEntry = true
		topInputEntry = false
	else 
		bottomInputEntry = false
		
	#print topInputEntry
	#print bottomInputEntry
	
	if topInputEntry
		inputEntry(event,placeholders[0])
		#print "top input"
		
	if bottomInputEntry
		#print "bottom input"
		inputEntry(event,placeholders[1])


# Input entry function
inputEntry = (event,text) ->
	e = event
	keyValue = String.fromCharCode(e.keyCode)
	e.preventDefault() # Prevents OS X making the annoying sound
	textLength = text.text.length
	
	# Delete 
	if e.keyCode == 8
		# Remove on character per delete key presse
		textContent = text.text
		text.text = textContent.substring 0, textContent.length - 1
	else if textLength < 27
		# Only add chars if it's not delete
		# Max length 
		text.text = text.text + keyValue

flow = new FlowComponent
flow.showNext(Starting_page)

# Create tap events for back buttons
backButtons = [Back_button,Back_button_1]

backButtons[0].onTap ->
	flow.showPrevious()
	resetInputs()
backButtons[1].onTap ->
	flow.showPrevious()
	# remove event listener on passcode page
	window.removeEventListener "keydown", passcodeEntry
	
buttons = [Lets_do_this_button,Set_your_passcode_button,Create_your_account_button]

pages = [Manual_field_entry,Set_your_passcode,Success]

# Create button states and tap events
for btn, index in buttons
	btn.states = 
		active:
			backgroundColor: "rgba(255,255,255,0.1)"
			animationOptions:
				time: 0.25
				curve: Bezier.easeInOut
		inactive:
			backgroundColor: "transparent"

buttons[0].onTapStart ->
	buttons[0].states.switch "active"

buttons[0].onTapEnd ->
	Utils.delay 0.25, ->
		flow.showNext(pages[0])
		buttons[0].states.switchInstant "inactive"
		
		# Use keypress to receieve case sensitive input
		window.addEventListener "keypress", inputEventProxy


buttons[1].onTapStart ->
	buttons[1].states.switch "active"

buttons[1].onTapEnd ->
	Utils.delay 0.25, ->
		flow.showNext(pages[1])
		buttons[1].states.switchInstant "inactive"
		
		# Remove event listeners for inputs
		window.removeEventListener "keypress", inputEventProxy
		
		# Bring the passcode inputs to life
		window.addEventListener "keydown", passcodeEntry

buttons[2].onTapStart ->
	buttons[2].states.switch "active"

buttons[2].onTapEnd ->
	Utils.delay 0.25, ->
		flow.showNext(pages[2])
		buttons[2].states.switchInstant "inactive"

# Inputs & place holder text
inputs = [full_name_input,mobile_number_input]
placeholders = [full_name_text,mobile_number_text]
labels = [full_name_label, mobile_number_label]

for input, index in inputs
	input.states =
		active:
			backgroundColor: "rgba(255,255,255,0.1)"
			y: input.y + 18
		textEntry:
			backgroundColor: "rgba(255,255,255,0.1)"
			y: input.y + 18
		twoActive: 
			backgroundColor: "rgba(255,255,255,0.1)"
			y: input.y + 35
		inactive:
			backgroundColor: "transparent"
			y: input.y
		animationOptions:
			time: 0.5
			curve: Spring
			
# Adaptive labels for usabilty
for label, index in labels 
	label.states =
		inactive: 
			opacity:0
			y:18
		active: 
			opacity:1
			y: label.y
		animationOptions:
			curve: Spring
			time: 0.5
	label.states.switch "inactive"

# Placeholders and flashing cursor
for text, index in placeholders
	text.states =
		active:
			text: ""
			opacity: 1
		inactive:
			text: text.text
			opacity: 0.6
		textEntered:
			opacity:1
		animationOptions:
				time: 0

inputs[0].onTap ->
	# only change states if there's no data in the field
	if placeholders[0].states.current.name == "default" || placeholders[0].states.current.name == "inactive"
		inputs[0].stateCycle "textEntry", "inactive"
		placeholders[0].stateCycle "active", "inactive"
		labels[0].stateCycle "active", "inactive"
	
	# Animation loop whilst input is active
	#placeholders[0].on Events.AnimationEnd, ->
		#if inputs[0].states.current.name == "active"
			#placeholders[0].stateCycle "active2", "active"
		#else 
			#return
			
	# Don't clear data if it exists already
	if placeholders[0].text.length < 1
		placeholders[0].states.switch "textEntered"

inputs[1].onTap ->

		inputs[1].stateCycle "twoActive", "inactive"
		placeholders[1].stateCycle "active", "inactive"
		labels[1].stateCycle "active", "inactive"
		
# Reset input states if tap on body
resetInputs = () ->
	if labels[0].states.current.name == "active" 
		inputs[0].states.switch "inactive"
		labels[0].states.switch "inactive"
		placeholders[0].states.switch "inactive"
	
	if labels[1].states.current.name == "active" 
		inputs[1].states.switch "inactive"
		labels[1].states.switch "inactive"
		placeholders[1].states.switch "inactive"

pageHeading.onTap ->
	resetInputs()

# Passcode inputs
passcodeInputs = [passcode_1,passcode_2,passcode_3,passcode_4]
passcodeText = [passcode_1_text,passcode_2_text,passcode_3_text,passcode_4_text]

for passcode in passcodeInputs
	passcode.states =
		active:
			backgroundColor: "rgba(255,255,255,0.1)"
			z: 9
			animationOptions:
				time: 0.25
				curve: Bezier.easeInOut
		inactive:
			backgroundColor: "transparent"
			z: 9
	passcode.states.switch "inactive"

for text in passcodeText
	text.states = 
		inactive: 
			opacity: 0
			text: ""
		active:
			opacity: 1
		animationOptions:
			time: 0.25
			curve: Bezier.easeInOut
	text.states.switch "inactive"

passcode_wrapper.onTap ->
	passcodeStateSwitch('cycle')

pageHeading_1.onTap ->
	passcodeStateSwitch('off')
