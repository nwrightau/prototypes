# Define and set custom device
Framer.Device.customize
	deviceType: Framer.Device.Type.Tablet
	devicePixelRatio: 1
	screenWidth: 768
	screenHeight: 1024
	deviceImage: "http://f.cl.ly/items/001L0v3c1f120t0p2z24/custom.png"
	deviceImageWidth: 800
	deviceImageHeight: 1214



# Import DVA JSON file and define parsed JSON Objects
data = JSON.parse Utils.domLoadDataSync "data/dva.json"

personTypes = data.personTypes
services = data.services


# Add find module
{ƒ,ƒƒ} = require 'findModule'

# Initilaise flow component
flow = new FlowComponent
flow.showNext(home)

input_who_you_are.index = 2
activePersonType = null 
activeServices = null

# Function to create personType list
createList = (object, artboard) ->
	listWrapper = new Layer
		backgroundColor: "#2d83ae"
		width: input_who_you_are.width
		x: Align.center
		y: 200
		opacity: 0
		height: 520
		index: 1
		parent: artboard
		name: "personTypeListWrapper"
	listWrapper.classList.add("personTypeListWrapper")
	
	listWrapper.states.active = 
		y: 355
		opacity:1
		options:
			curve: Spring
			time: 0.8
	
	listWrapper.states.inactive = 
		y: 200
		opacity: 0
		options:
			curve: Spring
			time: 0.6
	
	# A shortcut to set the height of each item
	cardHeight = 80
	# Loop over each airport
	for item, i in object
		listItem = new Layer        # Create a new Layer for each item
			parent: listWrapper   # Place layer inside scroll area
			height: cardHeight       # Set height of each item
			name: item.type
			y: i * cardHeight        # Offset each item with y position
			width: listWrapper.width - 60      # Make items full width
			backgroundColor: "transparent" # Set backgroundColor
			x: Align.center
			style: "border-bottom": "1px solid rgba(255,255,255,0.3)"
		listItem.classList.add("list-item")
		
		listText = new TextLayer    # New TextLayer for each airport
			parent: listItem         # Inside the layer we just made
			text: item.type       # Use our data to populate the text
			color: "#fff"            # Set text color
			fontSize: 25             # Set font size
			fontFamily: "OpenSans"
			textAlign:"center"
			width: listItem.width
			y: Align.center
			padding:
				top: 0
				left: 20
				bottom: 0
				right: 20
	

	lastListItem = ƒƒ('personTypeListWrapper > *')[object.length - 1]
	lastListItem.height = 100
	lastListItem.y = lastListItem.midY - 30
	#lastListItem.style = "border-color":"transparent"


# Function to create services list based on selected person type
createListfromKeys = (object, keys, artboard) ->
	
	# If ServiceListWrapper already exists, destroy it to prevent duplicates
	if ƒƒ('ServiceListWrapper')
		for layer in ƒƒ('ServiceListWrapper')
			layer.destroy()

	cardHeight = 80
	
	ServiceListWrapper = new Layer
		backgroundColor: "#2d83ae"
		width: input_who_you_are.width
		x: Align.center
		y: 280
		opacity: 1
		height: keys.length * 80
		index: 1
		parent: artboard
		name: "ServiceListWrapper"
		
	ServiceListWrapper.classList.add("ServiceListWrapper")
	
	# Retrieve service names from service object
	for key, i in keys
		listItem = new Layer        # Create a new Layer for each item
			parent: ServiceListWrapper   # Place layer inside scroll area
			height: cardHeight       # Set height of each item
			name: key
			y: i * cardHeight        # Offset each item with y position
			width: ServiceListWrapper.width - 60      # Make items full width
			backgroundColor: "transparent" # Set backgroundColor
			x: Align.center
			style: "border-bottom": "1px solid rgba(255,255,255,0.3)"
		listItem.classList.add("list-item")
		
		listText = new TextLayer    # New TextLayer for each airport
			parent: listItem         # Inside the layer we just made
			text: object[key].name       # Use our data to populate the text
			color: "#fff"            # Set text color
			fontSize: 25             # Set font size
			fontFamily: "OpenSans"
			textAlign:"center"
			width: listItem.width
			y: Align.center
			padding:
				top: 0
				left: 20
				bottom: 0
				right: 20
	
	# Position the button underneath the service list
	btn_whats_next.y = ƒ('ServiceListWrapper').y + ƒ('ServiceListWrapper').height + 20
# Service list code
btn_whats_next.onTap (event, layer) ->
	flow.showOverlayBottom(overlay_whats_next)

close_overlay.onTap (event, layer) ->
	flow.showPrevious() # Turns out this also closes overlay/modals!

# Define states
what_s_available_wrapper.states.active = 
	y: 79
	options:
		curve: Spring
		time: 0.8
	
what_s_available_wrapper.states.inactive =
	y: 374
	options:
		curve: Spring
		time: 0.8

text_who_you_are.states.inactive = 
	text: "Let’s start with who you are"
	
text_who_you_are.states.active = 
	text: "I am a..."

ico_arrow_who_you_are.states.active =
	opacity: 1
	
ico_arrow_who_you_are.states.inactive =
	opacity: 0
	
ico_close_who_you_are.states.active =
	opacity: 1

ico_close_who_you_are.states.inactive =
	opacity: 0


# Function to enable footers and headers
enableHeaderFooters = () ->
	if flow.header
		flow.header.visible = true
	if flow.footer
		flow.footer.visible = true

	# Not sure what the hell these are doing here but anyway
	if start_application
		start_application.visible = true
	if service_detail_footer
		service_detail_footer.visible = true

# Function to disable footers and headers
disableHeaderFooters = () ->
	if flow.header
		flow.header.visible = false
	if flow.footer
		flow.footer.visible = false
		
	# Not sure what the hell these are doing here but anyway
	if start_application
		start_application.visible = false 
	if service_detail_footer
		service_detail_footer.visible = false


# Function to reset to home screen
resetHome = () ->
	for layer in ƒƒ('personTypeListWrapper')	
		layer.destroy()	
	#for layer in ƒƒ('ServiceListWrapper')
		#layer.destroy()
	what_s_available_wrapper.stateSwitch("inactive")
	ico_arrow_who_you_are.stateSwitch("active")
	ico_close_who_you_are.stateSwitch("inactive")
	disableHeaderFooters()


input_who_you_are.onTap (event, layer) ->
	if ƒ("personTypeListWrapper")
		ƒ("personTypeListWrapper").destroy()
		
	createList personTypes, home
	
	ƒ("personTypeListWrapper").stateCycle "active","inactive"
	what_s_available_wrapper.stateCycle "active","inactive"
	
	text_who_you_are.stateCycle("active", "inactive", instant: true) 
	ico_arrow_who_you_are.stateCycle("inactive", "active", instant: true)
	ico_close_who_you_are.stateCycle("active", "inactive", instant: true)

	if what_s_available_wrapper.states.current.name == "inactive"
		# find all list wrapper layers and destroy them
		$('.personTypeListWrapper').remove()
			
	# Create onTap event for every item in the list wrapper
	for layer in ƒƒ('personTypeListWrapper > *')
		layer.onTap (event, layer) ->
			flow.showNext(service_list)
			
			# Reset state on first button, delay until transition finished
			Utils.delay 0.5, ->
				text_who_you_are.stateSwitch("inactive")
			
			# set activePersonType to the clicked layer name
			activePersonType = layer.name
			# Get service array from active personType
			for person in personTypes
				if person.type == activePersonType
					activeServices = person.services
					createListfromKeys services, activeServices, service_list
			# Service details pages
			
			# Create tap events for each layer
			for layer in ƒƒ('ServiceListWrapper > *')
				layer.onTap (event, layer) ->
					serviceKey = layer.name
					serviceName = services[serviceKey].name
					service_title.text = serviceName
					service_title.classList.add("service-detail-title")
					service_title.width = Screen.width
					service_title.x = 0
					
					bgOffset = service_detail_header.height + 15
					
					# Create text layer with description
					listText = new Layer
						parent: service_detail
						html: services[serviceKey].description
						color: "#303030"
						width: Screen.width - 170
						y: 25
						x: Align.center;
						backgroundColor: "none"
						name: serviceKey+"-text"
						height: Screen.height 
					listText.classList.add("service-detail-text")
					
					# Get height from HTML element
					textHeight = $('.service-detail-text').height()
					artBoardHeight = textHeight + 90
					
					# Set minimum artboard height
					if artBoardHeight < Screen.height
						artBoardHeight = Screen.height + 1
						
					listText.height = textHeight
					service_detail.height = artBoardHeight
					
					enableHeaderFooters()

					# Bind second back button
					# But don't bind it twice
					if service_detail_back.listeners(Events.Tap).length == 0
						service_detail_back.onTap (event, layer) ->
							#print flow.current.name
							if flow.current.name == "service-requirements"	
								flow.footer = service_detail_footer1
								enableHeaderFooters()
							if flow.current.name == "service-detail"
								$('.service-detail-text').remove()
								disableHeaderFooters()
							flow.showPrevious()
							
					flow.header = service_detail_header
					service_detail_footer1 = service_detail_footer.copy()
					flow.footer = service_detail_footer
					flow.showNext(service_detail)

					flow.scroll.backgroundColor = "#ffffff"
					
					if ƒ(serviceKey+"-requiredText")
						ƒ(serviceKey+"-requiredText").destroy()
					
					requiredText = new Layer
								parent: service_requirements
								html: services[serviceKey].requirements
								color: "#303030"
								width: Screen.width - 130
								y: 125
								x: Align.center;
								backgroundColor: "none"
								name: serviceKey+"-requiredText"
								height: Screen.height 
								backgroundColor: "none"
							requiredText.classList.add("service-detail-text")
							requiredText.classList.add("required-text")
					
					if btn_whats_involved.listeners(Events.Tap).length == 0
						btn_whats_involved.onTap (event,layer) ->
							flow.showNext(service_requirements)
							flow.scroll.backgroundColor = "#ffffff"
							flow.footer = start_application
							enableHeaderFooters()
							
							if start_application.listeners(Events.Tap).length == 0
								start_application.onTap (event,layer) ->
									flow.showNext(application)
									disableHeaderFooters()
									
									if application_back.listeners(Events.Tap).length == 0
										application_back.onTap (event,layer) ->
											flow.showPrevious()
											enableHeaderFooters()

# Bind first back button
if service_list_back.listeners(Events.Tap).length == 0
	service_list_back.onTap (event, layer) ->
		resetHome()
		flow.showPrevious()

btn_eligible.onTap (event,layer) ->
	resetHome()
	flow.showNext(eligibility_test)

eligibility_back.onTap (event,layer) ->
	enableHeaderFooters()
	flow.showPrevious()



