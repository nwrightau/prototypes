# Bring in CORS proxy for API
# https://gist.github.com/marckrenn/2bb78853e02af90c5904100a15398abe

Utils.CORSproxy = (url) ->

	# Detect local IPv4/IvP6 addresses
	# https://stackoverflow.com/a/11327345
	regexp = /(^127\.)|(^192\.168\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^::1$)|(^[fF][cCdD])/

	if regexp.test(window.location.hostname)
		return "http://#{window.location.host}/_server/proxy/#{url}"
	else
		return url

# Define and set custom device, init scrollComponent
Framer.Device.customize
	deviceType: Framer.Device.Type.Computer
	devicePixelRatio: 1
	screenWidth: 1440
	screenHeight: 900
	deviceImageWidth: 1440
	deviceImageHeight: 900

scroll = new ScrollComponent
	parent: Quote
	width: Products.width
	height: Framer.Device.screen.height - Header.height
	y: Header.height
	x: lh_sidebar.width
	scrollHorizontal: false
	mouseWheelEnabled: true

# Setup products
Products.x = 0
Products.y = 0
Products.parent = scroll.content

# Setup review for new page
Review.x = 0
Review.y = Framer.Device.screen.height + 200
Review.parent = scroll.content
Review.visible = false


# Set skeleton states

upperSkeletons = []
lowerSkeletons = []
gutter = 30
i = 0

for skeleton in [0..2]
	i++
	if i == 1 
		xPos = gutter
	else 
		xPos = skeleton * (product_upper_skeleton.width + gutter) + gutter
		
	upperSkeleton = product_upper_skeleton.copy()
	upperSkeleton.states =
	inactive: 
		visible:false
	active:
		x: xPos
		y: 105
		z: 9
		parent: Products

	upperSkeleton.stateSwitch "active"
	delete upperSkeleton.states.default	
	upperSkeletons.push(upperSkeleton)
	
	lowerSkeleton = product_lower_skeleton.copy()
	lowerSkeleton.states = 
		inactive:
			visible:false
		active:
			x: xPos
			y: 105 + product_upper_skeleton.height
			z: 9
			parent: Products
	
	lowerSkeleton.stateSwitch "active"
	delete lowerSkeleton.states.default
	lowerSkeletons.push(lowerSkeleton)



# Define API request params
apiUrl = "http://test.api.health.com.au/product/detail"
apiRequestDob = "10/12/2000"
apiRequestLifestage = "S"
apiRequestState = "VIC"
apiRequestRebateTier = 0
apiRequestPaymentFreq = 12

# 52/week
# 26/fn
# 12/month
# 1/year

apiProducts = ''

# Request products from API
xmlhttp = new XMLHttpRequest()   # new HttpRequest instance 

xmlhttp.open("POST", Utils.CORSproxy(apiUrl))

xmlhttp.setRequestHeader("Content-Type", "application/json")

xmlhttp.send(JSON.stringify({"dob":apiRequestDob,"partnerdob":"","lifeStage":apiRequestLifestage,"state":apiRequestState,"rebateTier":apiRequestRebateTier,"paymentFrequency":apiRequestPaymentFreq,"need":"XX","groupDiscount":"DISC-02","brand":"GMHBA","coverType":2,"effectiveFrom":"NOW","hospitalCoverLevel":"low","extrasCoverLevel":"low","abcSettingID":0,"pageCustomContentID":0,"isSalesPortal":false}))

#covertype  2 = combined, 1 = extras only, 0 = hospital only

# Split dollars/cents
currencySplit = (num) -> 
	amount = num.toString()
	dollars = amount.split('.')[0]
	cents = amount.split('.')[1] || '00'
	dollars = dollars.split('').reverse().join('').replace(/(\d{3}(?!$))/g, '$1,').split('').reverse().join('')
	# Add trailing 0s
	if cents.length <2
		cents = cents+'0'
	return [dollars,cents]
	
selectBtns = []

# populate products straight from GMHBA API
xmlhttp.onreadystatechange = () ->
	if this.readyState == 4 && this.status == 200
		apiResponse = JSON.parse xmlhttp.response	
		apiProducts = apiResponse.Products
		apiProductInfo = apiResponse.ProductInfoList
		#print apiProducts[0].IsVitality

		# Create products from for loop
		products = []
		i = -1
		gutter = 30
		textHeights = []
		lowerHeight = ''
		for product in apiProducts
			i++
			productWrapper = product_wrapper.copy()
			productWrapper.parent = Products
		
			if i == 0
				xPos = gutter
			else 
				xPos = i * (product_upper_skeleton.width + gutter) + gutter
			productWrapper.y = 105
			productWrapper.z = 999
			productWrapper.x = xPos
			
			products.push(productWrapper)
			
			for item in products[i].descendants
				if item.name == "productText"
					item.template = 
					"product_name": apiProducts[i].Name
					"product_desc" : apiProducts[i].SellingPoint
					"dollar": currencySplit(apiProducts[i].Rate.combinedNetAmount)[0]
					"cents": currencySplit(apiProducts[i].Rate.combinedNetAmount)[1]
				if item.name == "SellingPointTxt"
					productServices = ''
					for feature in apiProducts[i].Features
						title = apiProductInfo[feature.ProductInfoIndex].Text
						if title == 'Optical'
							productServices += title + ' *\n \n'
						else
							productServices += title + '\n \n'
					item.text = productServices
					textHeights.push(item.text.length)
				# Remove vitality layers if not required
				if item.name == "vitality-reveal-btn"
					if !apiProducts[i].IsVitality
						item.destroy()
				if item.name == "vitality-header"
					if !apiProducts[i].IsVitality
						item.destroy()
				if item.name == "vitality-reveal"
					if !apiProducts[i].IsVitality
						item.destroy()

		# Remove skeletons 
		setTimeout (->
			for upperSkeleton in upperSkeletons
				upperSkeleton.stateSwitch "inactive"
			
			for lowerSkeleton in lowerSkeletons
				lowerSkeleton.stateSwitch "inactive"
				
			# Vitality reveal drawer
			for product in products
				for item in product.descendants
					if item.name == "vitality-reveal"
						# Set vitality reveal states
						item.states =
							inactive: 
								height: 0
							active:
								height: vitality_reveal.height
							animationOptions:
								time: 0.35
						item.stateSwitch "inactive"
						delete item.states.default
						vitalityRevealContent = item
					
					if item.name == "SellingPointTxt"
						# Set selling point states
						item.states = 
							active: 
								y: item.y + vitality_reveal.height + 10
							inactive:
								y: item.y
							animationOptions:
								time: 0.35
						delete item.states.default
						VitalityTextBelow = item

					if item.name == "vitality-reveal-btn"
						# Toggle on tap
						item.onTap ->
							vitalityRevealContent.stateCycle()
							VitalityTextBelow.stateCycle()
							
					if item.name == "vitality-header"
						#Vitality info overlay
						flow = new FlowComponent
						flow.showNext(Quote, scroll: false)
						
						item.onTap ->
							flow.showOverlayCenter(overlay)
						
						close_overlay.onTap ->
							flow.showPrevious(Quote, scroll: false)
					if item.name == "select-btn"
						selectBtns.push(item)
			
			highestText = Math.max.apply null, textHeights
			lowerHeight = highestText + products[0].descendants[1].y * 2
			
			# Select button clicks
			selectBtns[1].onTap ->
				sidebar_price.states.switch "active"
				sidebar_price.template =
					"dollar": currencySplit(apiProducts[1].Rate.combinedNetAmount)[0]
					"cents": currencySplit(apiProducts[1].Rate.combinedNetAmount)[1]
				sidebar_product_name.states.switch "active"
				sidebar_product_name.template = 
					"product_name" : apiProducts[1].Name
					
				reviewProductText.template	= 
					"product_name": apiProducts[1].Name
					"product_desc" : apiProducts[1].SellingPoint
					"dollar": currencySplit(apiProducts[1].Rate.combinedNetAmount)[0]
					"cents": currencySplit(apiProducts[1].Rate.combinedNetAmount)[1]
				
				Products.animate
					properties:
						y: -Framer.Device.screen.height - 200
						
				Review.visible = true	
				Review.animate
					properties:
						y: 35

		), 1000
		

# Page component for Review

# RHS sidebar states
sidebar_product_name.states =
	inactive:
		opacity:0
	active: 
		opacity:1
	animationOptions:
		time:0
sidebar_product_name.states.switch "inactive"

sidebar_price.states =
	inactive:
		template: 
			"dollar": "0"
			"cents" : "00"
		color: "#d9dbde"
		y: sidebar_price.y - 100
	active:
		color: sidebar_price.color
		y: sidebar_price.y
	animationOptions:
		time:0
sidebar_price.states.switch "inactive"


UpgradeActive = false

Vitality_upsell.onTap ->
	if !UpgradeActive
		flow.showOverlayCenter(overlay)
	else
		Vitality_upsell_text.text = "Earn rewards by upgrading to a product with AIA Vitality, only $12.29 extra per week"
		Upgrade_btn.animate
			properties:
				height:63
				opacity:1
			time: 0.4
			curve: Bezier.easeInOut
		Review_lower_wrapper.animate
			properties:
				y: 384
			time: 0.4
			curve: Bezier.easeInOut
		sidebar_price.template =
			"dollar": currencySplit(apiProducts[1].Rate.combinedNetAmount)[0]
			"cents": currencySplit(apiProducts[1].Rate.combinedNetAmount)[1]
		sidebar_product_name.states.switch "active"
		sidebar_product_name.template = 
			"product_name" : apiProducts[1].Name
		Review_product.visible = true
		Vitality_review_product.visible = false

Upgrade_btn.onTap ->
	
	UpgradeActive = true
	
	# Remove Up-sell
	Vitality_upsell_wrapper.clip = true
	
	Review_lower_wrapper.animate
		properties:
			y: Review_lower_wrapper.y - Upgrade_btn.height - 15
		time: 0.4
		curve: Bezier.easeInOut
			
	Upgrade_btn.animate
		properties:
			height:0
			opacity:0
		time: 0.4
		curve: Bezier.easeInOut
		
	Vitality_upsell_text.text = "Thanks for your product change! If you didn’t mean to, click here to go back"

	# Change product
	sidebar_price.template =
		"dollar": currencySplit(apiProducts[0].Rate.combinedNetAmount)[0]
		"cents": currencySplit(apiProducts[0].Rate.combinedNetAmount)[1]
	sidebar_product_name.states.switch "active"
	sidebar_product_name.template = 
		"product_name" : apiProducts[0].Name
	
	Vitality_review_product.parent = Review_product.parent
	Vitality_review_product.x = Review_product.x
	Vitality_review_product.y = Review_product.y + 44
	Review_product.visible = false
	Vitality_review_product.visible = true
	
	Vitality_review_product_text.template = 
		"product_name": apiProducts[0].Name
		"product_desc" : apiProducts[0].SellingPoint
		"dollar": currencySplit(apiProducts[0].Rate.combinedNetAmount)[0]
		"cents": currencySplit(apiProducts[0].Rate.combinedNetAmount)[1]
