require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"DynamicLoader":[function(require,module,exports){

/*
DynamicLoader Module for FramerJS
https://github.com/LucienLee/framer-DynamicLoader/

Created by Lucien Lee (@luciendeer), Jan. 12th, 2016

DynamicLoader braeks the barriars between 3rd party web development libraries and Framer, which
help you load local, external stylesheets and scripts dynamically.

Add the following line to your project in Framer Studio.
	{DynamicLoader} = require 'DynamicLoader'

[Load one file]
DynamicLoader.add('script.js').then(->
 * when script.js loaded successfully
...
).catch(->
 * when script.js loaded failed
...
)

[Load file in series]
DynamicLoader.series(['one.js', 'two.css', ...]).then( successCallback, failCallback )

[Load file in parallel]
DynamicLoader.series(['one.js', 'two.css', ...]).then( successCallback, failCallback )
 */
exports.DynamicLoader = (function() {
  function DynamicLoader() {}

  DynamicLoader.add = function(url) {
    var promise;
    promise = new Promise(function(resolve, reject) {
      var file, loaded;
      if (url.substr(url.lastIndexOf('.')) === ".js") {
        loaded = Array.prototype.find.call(document.getElementsByTagName('script'), function(element) {
          if (element.getAttribute('src') === url) {
            return element;
          }
        });
        if (loaded !== void 0) {
          return resolve('have loaded');
        }
        file = document.createElement('script');
        file.src = url;
      } else if (url.substr(url.lastIndexOf('.')) === ".css") {
        loaded = Array.prototype.find.call(document.getElementsByTagName('link'), function(element) {
          if (element.getAttribute('rel') === url) {
            return element;
          }
        });
        if (loaded !== void 0) {
          return resolve('have loaded');
        }
        file = document.createElement('link');
        file.rel = "stylesheet";
        file.href = url;
      }
      file.addEventListener('load', function() {
        return resolve(file);
      });
      file.addEventListener('error', function() {
        return reject(file);
      });
      return document.body.appendChild(file);
    });
    return promise;
  };

  DynamicLoader.series = function(urls) {
    if (!Array.isArray(urls) || urls.length === 0) {
      throw "ERROR: NO URL IN ARRAY!";
    }
    return urls.reduce((function(_this) {
      return function(promise, url) {
        return promise.then(function() {
          return _this.add(url);
        });
      };
    })(this), Promise.resolve());
  };

  DynamicLoader.parallel = function(urls) {
    if (!Array.isArray(urls) || urls.length === 0) {
      throw "ERROR: NO URL IN ARRAY!";
    }
    return Promise.all(urls.map((function(_this) {
      return function(url) {
        return _this.add(url);
      };
    })(this)));
  };

  return DynamicLoader;

})();


},{}],"LayerTree":[function(require,module,exports){
module.exports = (function() {
  function exports() {}

  exports.buildLayers = function(file) {
    var layer, name, results;
    results = [];
    for (name in file) {
      layer = file[name];
      results.push(this.createChildNodes(layer));
    }
    return results;
  };

  exports.createChildNodes = function(layer) {
    var child, index, layerName, num, ref, results;
    ref = layer.children;
    results = [];
    for (index in ref) {
      child = ref[index];
      num = child.name.match(/\d+$/);
      if (parseInt(num)) {
        layerName = child.name.slice(0, child.name.indexOf(num));
        results.push(layer[layerName] = child);
      } else {
        results.push(layer[child.name] = child);
      }
    }
    return results;
  };

  return exports;

})();


},{}],"LayerTree":[function(require,module,exports){
var LayerTree;

LayerTree = (function() {
  function LayerTree() {}

  LayerTree.buildLayers = function(file) {
    var layer, name, results;
    results = [];
    for (name in file) {
      layer = file[name];
      results.push(this.createChildNodes(layer));
    }
    return results;
  };

  LayerTree.createChildNodes = function(layer) {
    var child, index, ref, results;
    ref = layer.children;
    results = [];
    for (index in ref) {
      child = ref[index];
      results.push(layer[child.name] = child);
    }
    return results;
  };

  return LayerTree;

})();

},{}],"findModule":[function(require,module,exports){
var _findAll, _getHierarchy, _match;

_getHierarchy = function(layer) {
  var a, i, len, ref, string;
  string = '';
  ref = layer.ancestors();
  for (i = 0, len = ref.length; i < len; i++) {
    a = ref[i];
    string = a.name + '>' + string;
  }
  return string = string + layer.name;
};

_match = function(hierarchy, string) {
  var regExp, regexString;
  string = string.replace(/\s*>\s*/g, '>');
  string = string.split('*').join('[^>]*');
  string = string.split(' ').join('(?:.*)>');
  string = string.split(',').join('$|');
  regexString = "(^|>)" + string + "$";
  regExp = new RegExp(regexString);
  return hierarchy.match(regExp);
};

_findAll = function(selector, fromLayer) {
  var layers, stringNeedsRegex;
  layers = Framer.CurrentContext._layers;
  if (selector != null) {
    stringNeedsRegex = _.find(['*', ' ', '>', ','], function(c) {
      return _.includes(selector, c);
    });
    if (!(stringNeedsRegex || fromLayer)) {
      return layers = _.filter(layers, function(layer) {
        if (layer.name === selector) {
          return true;
        }
      });
    } else {
      return layers = _.filter(layers, function(layer) {
        var hierarchy;
        hierarchy = _getHierarchy(layer);
        if (fromLayer != null) {
          return _match(hierarchy, fromLayer.name + ' ' + selector);
        } else {
          return _match(hierarchy, selector);
        }
      });
    }
  } else {
    return layers;
  }
};

exports.Find = function(selector, fromLayer) {
  return _findAll(selector, fromLayer)[0];
};

exports.ƒ = function(selector, fromLayer) {
  return _findAll(selector, fromLayer)[0];
};

exports.FindAll = function(selector, fromLayer) {
  return _findAll(selector, fromLayer);
};

exports.ƒƒ = function(selector, fromLayer) {
  return _findAll(selector, fromLayer);
};

Layer.prototype.find = function(selector, fromLayer) {
  return _findAll(selector, this)[0];
};

Layer.prototype.ƒ = function(selector, fromLayer) {
  return _findAll(selector, this)[0];
};

Layer.prototype.findAll = function(selector, fromLayer) {
  return _findAll(selector, this);
};

Layer.prototype.ƒƒ = function(selector, fromLayer) {
  return _findAll(selector, this);
};


},{}],"myModule":[function(require,module,exports){
exports.myVar = "myVariable";

exports.myFunction = function() {
  return print("myFunction is running");
};

exports.myArray = [1, 2, 3];


},{}]},{},[])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnJhbWVyLm1vZHVsZXMuanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL1VzZXJzL25hdGhhbi9EZXNrdG9wL1Byb3RvdHlwZXMvR01IQkEvVml0YWxpdHktcXVvdGUtbW9iaWxlLmZyYW1lci9tb2R1bGVzL215TW9kdWxlLmNvZmZlZSIsIi4uLy4uLy4uLy4uLy4uL1VzZXJzL25hdGhhbi9EZXNrdG9wL1Byb3RvdHlwZXMvR01IQkEvVml0YWxpdHktcXVvdGUtbW9iaWxlLmZyYW1lci9tb2R1bGVzL2ZpbmRNb2R1bGUuY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vVXNlcnMvbmF0aGFuL0Rlc2t0b3AvUHJvdG90eXBlcy9HTUhCQS9WaXRhbGl0eS1xdW90ZS1tb2JpbGUuZnJhbWVyL21vZHVsZXMvTGF5ZXJUcmVlLmpzIiwiLi4vLi4vLi4vLi4vLi4vVXNlcnMvbmF0aGFuL0Rlc2t0b3AvUHJvdG90eXBlcy9HTUhCQS9WaXRhbGl0eS1xdW90ZS1tb2JpbGUuZnJhbWVyL21vZHVsZXMvTGF5ZXJUcmVlLmNvZmZlZSIsIi4uLy4uLy4uLy4uLy4uL1VzZXJzL25hdGhhbi9EZXNrdG9wL1Byb3RvdHlwZXMvR01IQkEvVml0YWxpdHktcXVvdGUtbW9iaWxlLmZyYW1lci9tb2R1bGVzL0R5bmFtaWNMb2FkZXIuY29mZmVlIiwibm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIjIEFkZCB0aGUgZm9sbG93aW5nIGxpbmUgdG8geW91ciBwcm9qZWN0IGluIEZyYW1lciBTdHVkaW8uXG4jIG15TW9kdWxlID0gcmVxdWlyZSBcIm15TW9kdWxlXCJcbiMgUmVmZXJlbmNlIHRoZSBjb250ZW50cyBieSBuYW1lLCBsaWtlIG15TW9kdWxlLm15RnVuY3Rpb24oKSBvciBteU1vZHVsZS5teVZhclxuXG5leHBvcnRzLm15VmFyID0gXCJteVZhcmlhYmxlXCJcblxuZXhwb3J0cy5teUZ1bmN0aW9uID0gLT5cblx0cHJpbnQgXCJteUZ1bmN0aW9uIGlzIHJ1bm5pbmdcIlxuXG5leHBvcnRzLm15QXJyYXkgPSBbMSwgMiwgM11cbiIsIl9nZXRIaWVyYXJjaHkgPSAobGF5ZXIpIC0+XG4gIHN0cmluZyA9ICcnXG4gIGZvciBhIGluIGxheWVyLmFuY2VzdG9ycygpXG4gICAgc3RyaW5nID0gYS5uYW1lKyc+JytzdHJpbmdcbiAgcmV0dXJuIHN0cmluZyA9IHN0cmluZytsYXllci5uYW1lXG5cbl9tYXRjaCA9IChoaWVyYXJjaHksIHN0cmluZykgLT5cbiAgIyBwcmVwYXJlIHJlZ2V4IHRva2Vuc1xuICBzdHJpbmcgPSBzdHJpbmcucmVwbGFjZSgvXFxzKj5cXHMqL2csJz4nKSAjIGNsZWFuIHVwIHNwYWNlcyBhcm91bmQgYXJyb3dzXG4gIHN0cmluZyA9IHN0cmluZy5zcGxpdCgnKicpLmpvaW4oJ1tePl0qJykgIyBhc3RlcmlrcyBhcyBsYXllciBuYW1lIHdpbGRjYXJkXG4gIHN0cmluZyA9IHN0cmluZy5zcGxpdCgnICcpLmpvaW4oJyg/Oi4qKT4nKSAjIHNwYWNlIGFzIHN0cnVjdHVyZSB3aWxkY2FyZFxuICBzdHJpbmcgPSBzdHJpbmcuc3BsaXQoJywnKS5qb2luKCckfCcpICMgYWxsb3cgbXVsdGlwbGUgc2VhcmNoZXMgdXNpbmcgY29tbWFcbiAgcmVnZXhTdHJpbmcgPSBcIihefD4pXCIrc3RyaW5nK1wiJFwiICMgYWx3YXlzIGJvdHRvbSBsYXllciwgbWF5YmUgcGFydCBvZiBoaWVyYXJjaHlcblxuICByZWdFeHAgPSBuZXcgUmVnRXhwKHJlZ2V4U3RyaW5nKSBcbiAgcmV0dXJuIGhpZXJhcmNoeS5tYXRjaChyZWdFeHApXG5cbl9maW5kQWxsID0gKHNlbGVjdG9yLCBmcm9tTGF5ZXIpIC0+XG4gIGxheWVycyA9IEZyYW1lci5DdXJyZW50Q29udGV4dC5fbGF5ZXJzXG5cbiAgaWYgc2VsZWN0b3I/XG4gICAgc3RyaW5nTmVlZHNSZWdleCA9IF8uZmluZCBbJyonLCcgJywnPicsJywnXSwgKGMpIC0+IF8uaW5jbHVkZXMgc2VsZWN0b3IsY1xuICAgIHVubGVzcyBzdHJpbmdOZWVkc1JlZ2V4IG9yIGZyb21MYXllclxuICAgICAgbGF5ZXJzID0gXy5maWx0ZXIgbGF5ZXJzLCAobGF5ZXIpIC0+IFxuICAgICAgICBpZiBsYXllci5uYW1lIGlzIHNlbGVjdG9yIHRoZW4gdHJ1ZVxuICAgIGVsc2VcbiAgICAgIGxheWVycyA9IF8uZmlsdGVyIGxheWVycywgKGxheWVyKSAtPlxuICAgICAgICAgIGhpZXJhcmNoeSA9IF9nZXRIaWVyYXJjaHkobGF5ZXIpXG4gICAgICAgICAgaWYgZnJvbUxheWVyP1xuICAgICAgICAgICAgX21hdGNoKGhpZXJhcmNoeSwgZnJvbUxheWVyLm5hbWUrJyAnK3NlbGVjdG9yKVxuICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIF9tYXRjaChoaWVyYXJjaHksIHNlbGVjdG9yKVxuICBlbHNlXG4gICAgbGF5ZXJzXG5cblxuIyBHbG9iYWxcbmV4cG9ydHMuRmluZCAgICA9IChzZWxlY3RvciwgZnJvbUxheWVyKSAtPiBfZmluZEFsbChzZWxlY3RvciwgZnJvbUxheWVyKVswXVxuZXhwb3J0cy7GkiAgICAgICA9IChzZWxlY3RvciwgZnJvbUxheWVyKSAtPiBfZmluZEFsbChzZWxlY3RvciwgZnJvbUxheWVyKVswXVxuXG5leHBvcnRzLkZpbmRBbGwgPSAoc2VsZWN0b3IsIGZyb21MYXllcikgLT4gX2ZpbmRBbGwoc2VsZWN0b3IsIGZyb21MYXllcilcbmV4cG9ydHMuxpLGkiAgICAgID0gKHNlbGVjdG9yLCBmcm9tTGF5ZXIpIC0+IF9maW5kQWxsKHNlbGVjdG9yLCBmcm9tTGF5ZXIpXG5cbiMgTWV0aG9kc1xuTGF5ZXI6OmZpbmQgICAgID0gKHNlbGVjdG9yLCBmcm9tTGF5ZXIpIC0+IF9maW5kQWxsKHNlbGVjdG9yLCBAKVswXVxuTGF5ZXI6OsaSICAgICAgICA9IChzZWxlY3RvciwgZnJvbUxheWVyKSAtPiBfZmluZEFsbChzZWxlY3RvciwgQClbMF1cblxuTGF5ZXI6OmZpbmRBbGwgID0gKHNlbGVjdG9yLCBmcm9tTGF5ZXIpIC0+IF9maW5kQWxsKHNlbGVjdG9yLCBAKVxuTGF5ZXI6OsaSxpIgICAgICAgPSAoc2VsZWN0b3IsIGZyb21MYXllcikgLT4gX2ZpbmRBbGwoc2VsZWN0b3IsIEApIiwidmFyIExheWVyVHJlZTtcblxuTGF5ZXJUcmVlID0gKGZ1bmN0aW9uKCkge1xuICBmdW5jdGlvbiBMYXllclRyZWUoKSB7fVxuXG4gIExheWVyVHJlZS5idWlsZExheWVycyA9IGZ1bmN0aW9uKGZpbGUpIHtcbiAgICB2YXIgbGF5ZXIsIG5hbWUsIHJlc3VsdHM7XG4gICAgcmVzdWx0cyA9IFtdO1xuICAgIGZvciAobmFtZSBpbiBmaWxlKSB7XG4gICAgICBsYXllciA9IGZpbGVbbmFtZV07XG4gICAgICByZXN1bHRzLnB1c2godGhpcy5jcmVhdGVDaGlsZE5vZGVzKGxheWVyKSk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHRzO1xuICB9O1xuXG4gIExheWVyVHJlZS5jcmVhdGVDaGlsZE5vZGVzID0gZnVuY3Rpb24obGF5ZXIpIHtcbiAgICB2YXIgY2hpbGQsIGluZGV4LCByZWYsIHJlc3VsdHM7XG4gICAgcmVmID0gbGF5ZXIuY2hpbGRyZW47XG4gICAgcmVzdWx0cyA9IFtdO1xuICAgIGZvciAoaW5kZXggaW4gcmVmKSB7XG4gICAgICBjaGlsZCA9IHJlZltpbmRleF07XG4gICAgICByZXN1bHRzLnB1c2gobGF5ZXJbY2hpbGQubmFtZV0gPSBjaGlsZCk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHRzO1xuICB9O1xuXG4gIHJldHVybiBMYXllclRyZWU7XG5cbn0pKCk7XG4iLCJjbGFzcyBtb2R1bGUuZXhwb3J0c1xuXHRAYnVpbGRMYXllcnMgPSAoZmlsZSkgLT5cblx0XHRmb3IgbmFtZSwgbGF5ZXIgb2YgZmlsZVxuXHRcdFx0QGNyZWF0ZUNoaWxkTm9kZXMgbGF5ZXJcblxuXHRAY3JlYXRlQ2hpbGROb2RlcyA9IChsYXllcikgLT5cblx0XHRmb3IgaW5kZXgsIGNoaWxkIG9mIGxheWVyLmNoaWxkcmVuXG5cdFx0XHRcblx0XHRcdCNjaGVjayBpZiBsYXllciBuYW1lIGVuZHMgaW4gbnVtYmVyLiBJZiBzbywgcmVtb3ZlIGl0LlxuXHRcdFx0bnVtID0gY2hpbGQubmFtZS5tYXRjaCgvXFxkKyQvKVxuXHRcdFx0aWYgcGFyc2VJbnQobnVtKVxuXHRcdFx0XHRsYXllck5hbWUgPSBjaGlsZC5uYW1lLnNsaWNlKDAsIGNoaWxkLm5hbWUuaW5kZXhPZihudW0pKVxuXHRcdFx0XHRsYXllcltsYXllck5hbWVdID0gY2hpbGRcblx0XHRcdGVsc2Vcblx0XHRcdFx0bGF5ZXJbY2hpbGQubmFtZV0gPSBjaGlsZFxuIiwiIyMjXG5EeW5hbWljTG9hZGVyIE1vZHVsZSBmb3IgRnJhbWVySlNcbmh0dHBzOi8vZ2l0aHViLmNvbS9MdWNpZW5MZWUvZnJhbWVyLUR5bmFtaWNMb2FkZXIvXG5cbkNyZWF0ZWQgYnkgTHVjaWVuIExlZSAoQGx1Y2llbmRlZXIpLCBKYW4uIDEydGgsIDIwMTZcblxuRHluYW1pY0xvYWRlciBicmFla3MgdGhlIGJhcnJpYXJzIGJldHdlZW4gM3JkIHBhcnR5IHdlYiBkZXZlbG9wbWVudCBsaWJyYXJpZXMgYW5kIEZyYW1lciwgd2hpY2hcbmhlbHAgeW91IGxvYWQgbG9jYWwsIGV4dGVybmFsIHN0eWxlc2hlZXRzIGFuZCBzY3JpcHRzIGR5bmFtaWNhbGx5LlxuXG5BZGQgdGhlIGZvbGxvd2luZyBsaW5lIHRvIHlvdXIgcHJvamVjdCBpbiBGcmFtZXIgU3R1ZGlvLlxuXHR7RHluYW1pY0xvYWRlcn0gPSByZXF1aXJlICdEeW5hbWljTG9hZGVyJ1xuXG5bTG9hZCBvbmUgZmlsZV1cbkR5bmFtaWNMb2FkZXIuYWRkKCdzY3JpcHQuanMnKS50aGVuKC0+XG4jIHdoZW4gc2NyaXB0LmpzIGxvYWRlZCBzdWNjZXNzZnVsbHlcbi4uLlxuKS5jYXRjaCgtPlxuIyB3aGVuIHNjcmlwdC5qcyBsb2FkZWQgZmFpbGVkXG4uLi5cbilcblxuW0xvYWQgZmlsZSBpbiBzZXJpZXNdXG5EeW5hbWljTG9hZGVyLnNlcmllcyhbJ29uZS5qcycsICd0d28uY3NzJywgLi4uXSkudGhlbiggc3VjY2Vzc0NhbGxiYWNrLCBmYWlsQ2FsbGJhY2sgKVxuXG5bTG9hZCBmaWxlIGluIHBhcmFsbGVsXVxuRHluYW1pY0xvYWRlci5zZXJpZXMoWydvbmUuanMnLCAndHdvLmNzcycsIC4uLl0pLnRoZW4oIHN1Y2Nlc3NDYWxsYmFjaywgZmFpbENhbGxiYWNrIClcblxuIyMjXG5cblxuXG5cbmNsYXNzIGV4cG9ydHMuRHluYW1pY0xvYWRlclxuXG5cdCMgUHJvbWlzaWZ5IHNpbmdsZSBkeW5hbWljIHNjcmlwdCBsb2FkaW5nXG5cdEBhZGQgPSAodXJsKSAtPlxuXHRcdHByb21pc2UgPSBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSAtPlxuXHRcdFx0aWYgdXJsLnN1YnN0ciggdXJsLmxhc3RJbmRleE9mKCcuJykgKSBpcyBcIi5qc1wiXG5cdFx0XHRcdCMgbG9hZCBzY3JpcHQgb25jZVxuXHRcdFx0XHRsb2FkZWQgPSBBcnJheS5wcm90b3R5cGUuZmluZC5jYWxsIGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdzY3JpcHQnKSwgKGVsZW1lbnQpIC0+XG5cdFx0XHRcdFx0aWYgZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3NyYycpIGlzIHVybCB0aGVuIHJldHVybiBlbGVtZW50XG5cdFx0XHRcdGlmIGxvYWRlZCBpc250IHVuZGVmaW5lZCB0aGVuIHJldHVybiByZXNvbHZlICdoYXZlIGxvYWRlZCdcblxuXHRcdFx0XHRmaWxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCAnc2NyaXB0J1xuXHRcdFx0XHRmaWxlLnNyYyA9IHVybFxuXHRcdFx0ZWxzZSBpZiB1cmwuc3Vic3RyKCB1cmwubGFzdEluZGV4T2YoJy4nKSApIGlzIFwiLmNzc1wiXG5cdFx0XHRcdCMgbG9hZCBzdHlsZSBvbmNlXG5cdFx0XHRcdGxvYWRlZCA9IEFycmF5LnByb3RvdHlwZS5maW5kLmNhbGwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2xpbmsnKSwgKGVsZW1lbnQpIC0+XG5cdFx0XHRcdFx0aWYgZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3JlbCcpIGlzIHVybCB0aGVuIHJldHVybiBlbGVtZW50XG5cdFx0XHRcdGlmIGxvYWRlZCBpc250IHVuZGVmaW5lZCB0aGVuIHJldHVybiByZXNvbHZlICdoYXZlIGxvYWRlZCdcblxuXHRcdFx0XHRmaWxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCAnbGluaydcblx0XHRcdFx0ZmlsZS5yZWwgPSBcInN0eWxlc2hlZXRcIlxuXHRcdFx0XHRmaWxlLmhyZWYgPSB1cmxcblxuXHRcdFx0ZmlsZS5hZGRFdmVudExpc3RlbmVyICdsb2FkJywgLT5cblx0XHRcdFx0cmVzb2x2ZSBmaWxlXG5cdFx0XHRmaWxlLmFkZEV2ZW50TGlzdGVuZXIgJ2Vycm9yJywgLT5cblx0XHRcdFx0cmVqZWN0IGZpbGVcblx0XHRcdGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQgZmlsZVxuXHRcdClcblxuXHRcdHJldHVybiBwcm9taXNlXG5cblx0IyBEeW5hbWljIGZpbGUgbG9hZGluZyBpbiBzZXJpZXNcblx0QHNlcmllcyA9ICh1cmxzKSAtPlxuXHRcdGlmICFBcnJheS5pc0FycmF5KHVybHMpIG9yIHVybHMubGVuZ3RoIGlzIDAgdGhlbiB0aHJvdyBcIkVSUk9SOiBOTyBVUkwgSU4gQVJSQVkhXCJcblxuXHRcdHJldHVybiB1cmxzLnJlZHVjZShcblx0XHRcdChwcm9taXNlLCB1cmwpID0+XG5cdFx0XHRcdHJldHVybiBwcm9taXNlLnRoZW4oID0+IEBhZGQodXJsKSApXG5cdFx0XHQsXG5cdFx0XHRQcm9taXNlLnJlc29sdmUoKSlcblxuXHQjIER5bmFtaWMgZmlsZSBsb2FkaW5nIGluIHBhcmFsbGVsXG5cdEBwYXJhbGxlbCA9ICh1cmxzKSAtPlxuXHRcdGlmICFBcnJheS5pc0FycmF5KHVybHMpIG9yIHVybHMubGVuZ3RoIGlzIDAgdGhlbiB0aHJvdyBcIkVSUk9SOiBOTyBVUkwgSU4gQVJSQVkhXCJcblxuXHRcdFByb21pc2UuYWxsKFxuXHRcdFx0dXJscy5tYXAoICh1cmwpID0+XG5cdFx0XHRcdHJldHVybiBAYWRkKHVybClcblx0XHQpKSIsIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBS0FBOztBREFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQ00sT0FBTyxDQUFDOzs7RUFHYixhQUFDLENBQUEsR0FBRCxHQUFPLFNBQUMsR0FBRDtBQUNOLFFBQUE7SUFBQSxPQUFBLEdBQWMsSUFBQSxPQUFBLENBQVEsU0FBQyxPQUFELEVBQVUsTUFBVjtBQUNyQixVQUFBO01BQUEsSUFBRyxHQUFHLENBQUMsTUFBSixDQUFZLEdBQUcsQ0FBQyxXQUFKLENBQWdCLEdBQWhCLENBQVosQ0FBQSxLQUFzQyxLQUF6QztRQUVDLE1BQUEsR0FBUyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFyQixDQUEwQixRQUFRLENBQUMsb0JBQVQsQ0FBOEIsUUFBOUIsQ0FBMUIsRUFBbUUsU0FBQyxPQUFEO1VBQzNFLElBQUcsT0FBTyxDQUFDLFlBQVIsQ0FBcUIsS0FBckIsQ0FBQSxLQUErQixHQUFsQztBQUEyQyxtQkFBTyxRQUFsRDs7UUFEMkUsQ0FBbkU7UUFFVCxJQUFHLE1BQUEsS0FBWSxNQUFmO0FBQThCLGlCQUFPLE9BQUEsQ0FBUSxhQUFSLEVBQXJDOztRQUVBLElBQUEsR0FBTyxRQUFRLENBQUMsYUFBVCxDQUF1QixRQUF2QjtRQUNQLElBQUksQ0FBQyxHQUFMLEdBQVcsSUFQWjtPQUFBLE1BUUssSUFBRyxHQUFHLENBQUMsTUFBSixDQUFZLEdBQUcsQ0FBQyxXQUFKLENBQWdCLEdBQWhCLENBQVosQ0FBQSxLQUFzQyxNQUF6QztRQUVKLE1BQUEsR0FBUyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFyQixDQUEwQixRQUFRLENBQUMsb0JBQVQsQ0FBOEIsTUFBOUIsQ0FBMUIsRUFBaUUsU0FBQyxPQUFEO1VBQ3pFLElBQUcsT0FBTyxDQUFDLFlBQVIsQ0FBcUIsS0FBckIsQ0FBQSxLQUErQixHQUFsQztBQUEyQyxtQkFBTyxRQUFsRDs7UUFEeUUsQ0FBakU7UUFFVCxJQUFHLE1BQUEsS0FBWSxNQUFmO0FBQThCLGlCQUFPLE9BQUEsQ0FBUSxhQUFSLEVBQXJDOztRQUVBLElBQUEsR0FBTyxRQUFRLENBQUMsYUFBVCxDQUF1QixNQUF2QjtRQUNQLElBQUksQ0FBQyxHQUFMLEdBQVc7UUFDWCxJQUFJLENBQUMsSUFBTCxHQUFZLElBUlI7O01BVUwsSUFBSSxDQUFDLGdCQUFMLENBQXNCLE1BQXRCLEVBQThCLFNBQUE7ZUFDN0IsT0FBQSxDQUFRLElBQVI7TUFENkIsQ0FBOUI7TUFFQSxJQUFJLENBQUMsZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsU0FBQTtlQUM5QixNQUFBLENBQU8sSUFBUDtNQUQ4QixDQUEvQjthQUVBLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBZCxDQUEwQixJQUExQjtJQXZCcUIsQ0FBUjtBQTBCZCxXQUFPO0VBM0JEOztFQThCUCxhQUFDLENBQUEsTUFBRCxHQUFVLFNBQUMsSUFBRDtJQUNULElBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTixDQUFjLElBQWQsQ0FBRCxJQUF3QixJQUFJLENBQUMsTUFBTCxLQUFlLENBQTFDO0FBQWlELFlBQU0sMEJBQXZEOztBQUVBLFdBQU8sSUFBSSxDQUFDLE1BQUwsQ0FDTixDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsT0FBRCxFQUFVLEdBQVY7QUFDQyxlQUFPLE9BQU8sQ0FBQyxJQUFSLENBQWMsU0FBQTtpQkFBRyxLQUFDLENBQUEsR0FBRCxDQUFLLEdBQUw7UUFBSCxDQUFkO01BRFI7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBRE0sRUFJTixPQUFPLENBQUMsT0FBUixDQUFBLENBSk07RUFIRTs7RUFVVixhQUFDLENBQUEsUUFBRCxHQUFZLFNBQUMsSUFBRDtJQUNYLElBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTixDQUFjLElBQWQsQ0FBRCxJQUF3QixJQUFJLENBQUMsTUFBTCxLQUFlLENBQTFDO0FBQWlELFlBQU0sMEJBQXZEOztXQUVBLE9BQU8sQ0FBQyxHQUFSLENBQ0MsSUFBSSxDQUFDLEdBQUwsQ0FBVSxDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsR0FBRDtBQUNULGVBQU8sS0FBQyxDQUFBLEdBQUQsQ0FBSyxHQUFMO01BREU7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVYsQ0FERDtFQUhXOzs7Ozs7OztBRDNFUCxNQUFNLENBQUM7OztFQUNaLE9BQUMsQ0FBQSxXQUFELEdBQWUsU0FBQyxJQUFEO0FBQ2QsUUFBQTtBQUFBO1NBQUEsWUFBQTs7bUJBQ0MsSUFBQyxDQUFBLGdCQUFELENBQWtCLEtBQWxCO0FBREQ7O0VBRGM7O0VBSWYsT0FBQyxDQUFBLGdCQUFELEdBQW9CLFNBQUMsS0FBRDtBQUNuQixRQUFBO0FBQUE7QUFBQTtTQUFBLFlBQUE7O01BR0MsR0FBQSxHQUFNLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBWCxDQUFpQixNQUFqQjtNQUNOLElBQUcsUUFBQSxDQUFTLEdBQVQsQ0FBSDtRQUNDLFNBQUEsR0FBWSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQVgsQ0FBaUIsQ0FBakIsRUFBb0IsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFYLENBQW1CLEdBQW5CLENBQXBCO3FCQUNaLEtBQU0sQ0FBQSxTQUFBLENBQU4sR0FBbUIsT0FGcEI7T0FBQSxNQUFBO3FCQUlDLEtBQU0sQ0FBQSxLQUFLLENBQUMsSUFBTixDQUFOLEdBQW9CLE9BSnJCOztBQUpEOztFQURtQjs7Ozs7Ozs7QURMckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBRDdCQSxJQUFBOztBQUFBLGFBQUEsR0FBZ0IsU0FBQyxLQUFEO0FBQ2QsTUFBQTtFQUFBLE1BQUEsR0FBUztBQUNUO0FBQUEsT0FBQSxxQ0FBQTs7SUFDRSxNQUFBLEdBQVMsQ0FBQyxDQUFDLElBQUYsR0FBTyxHQUFQLEdBQVc7QUFEdEI7QUFFQSxTQUFPLE1BQUEsR0FBUyxNQUFBLEdBQU8sS0FBSyxDQUFDO0FBSmY7O0FBTWhCLE1BQUEsR0FBUyxTQUFDLFNBQUQsRUFBWSxNQUFaO0FBRVAsTUFBQTtFQUFBLE1BQUEsR0FBUyxNQUFNLENBQUMsT0FBUCxDQUFlLFVBQWYsRUFBMEIsR0FBMUI7RUFDVCxNQUFBLEdBQVMsTUFBTSxDQUFDLEtBQVAsQ0FBYSxHQUFiLENBQWlCLENBQUMsSUFBbEIsQ0FBdUIsT0FBdkI7RUFDVCxNQUFBLEdBQVMsTUFBTSxDQUFDLEtBQVAsQ0FBYSxHQUFiLENBQWlCLENBQUMsSUFBbEIsQ0FBdUIsU0FBdkI7RUFDVCxNQUFBLEdBQVMsTUFBTSxDQUFDLEtBQVAsQ0FBYSxHQUFiLENBQWlCLENBQUMsSUFBbEIsQ0FBdUIsSUFBdkI7RUFDVCxXQUFBLEdBQWMsT0FBQSxHQUFRLE1BQVIsR0FBZTtFQUU3QixNQUFBLEdBQWEsSUFBQSxNQUFBLENBQU8sV0FBUDtBQUNiLFNBQU8sU0FBUyxDQUFDLEtBQVYsQ0FBZ0IsTUFBaEI7QUFUQTs7QUFXVCxRQUFBLEdBQVcsU0FBQyxRQUFELEVBQVcsU0FBWDtBQUNULE1BQUE7RUFBQSxNQUFBLEdBQVMsTUFBTSxDQUFDLGNBQWMsQ0FBQztFQUUvQixJQUFHLGdCQUFIO0lBQ0UsZ0JBQUEsR0FBbUIsQ0FBQyxDQUFDLElBQUYsQ0FBTyxDQUFDLEdBQUQsRUFBSyxHQUFMLEVBQVMsR0FBVCxFQUFhLEdBQWIsQ0FBUCxFQUEwQixTQUFDLENBQUQ7YUFBTyxDQUFDLENBQUMsUUFBRixDQUFXLFFBQVgsRUFBb0IsQ0FBcEI7SUFBUCxDQUExQjtJQUNuQixJQUFBLENBQUEsQ0FBTyxnQkFBQSxJQUFvQixTQUEzQixDQUFBO2FBQ0UsTUFBQSxHQUFTLENBQUMsQ0FBQyxNQUFGLENBQVMsTUFBVCxFQUFpQixTQUFDLEtBQUQ7UUFDeEIsSUFBRyxLQUFLLENBQUMsSUFBTixLQUFjLFFBQWpCO2lCQUErQixLQUEvQjs7TUFEd0IsQ0FBakIsRUFEWDtLQUFBLE1BQUE7YUFJRSxNQUFBLEdBQVMsQ0FBQyxDQUFDLE1BQUYsQ0FBUyxNQUFULEVBQWlCLFNBQUMsS0FBRDtBQUN0QixZQUFBO1FBQUEsU0FBQSxHQUFZLGFBQUEsQ0FBYyxLQUFkO1FBQ1osSUFBRyxpQkFBSDtpQkFDRSxNQUFBLENBQU8sU0FBUCxFQUFrQixTQUFTLENBQUMsSUFBVixHQUFlLEdBQWYsR0FBbUIsUUFBckMsRUFERjtTQUFBLE1BQUE7aUJBR0UsTUFBQSxDQUFPLFNBQVAsRUFBa0IsUUFBbEIsRUFIRjs7TUFGc0IsQ0FBakIsRUFKWDtLQUZGO0dBQUEsTUFBQTtXQWFFLE9BYkY7O0FBSFM7O0FBb0JYLE9BQU8sQ0FBQyxJQUFSLEdBQWtCLFNBQUMsUUFBRCxFQUFXLFNBQVg7U0FBeUIsUUFBQSxDQUFTLFFBQVQsRUFBbUIsU0FBbkIsQ0FBOEIsQ0FBQSxDQUFBO0FBQXZEOztBQUNsQixPQUFPLENBQUMsQ0FBUixHQUFrQixTQUFDLFFBQUQsRUFBVyxTQUFYO1NBQXlCLFFBQUEsQ0FBUyxRQUFULEVBQW1CLFNBQW5CLENBQThCLENBQUEsQ0FBQTtBQUF2RDs7QUFFbEIsT0FBTyxDQUFDLE9BQVIsR0FBa0IsU0FBQyxRQUFELEVBQVcsU0FBWDtTQUF5QixRQUFBLENBQVMsUUFBVCxFQUFtQixTQUFuQjtBQUF6Qjs7QUFDbEIsT0FBTyxDQUFDLEVBQVIsR0FBa0IsU0FBQyxRQUFELEVBQVcsU0FBWDtTQUF5QixRQUFBLENBQVMsUUFBVCxFQUFtQixTQUFuQjtBQUF6Qjs7QUFHbEIsS0FBSyxDQUFBLFNBQUUsQ0FBQSxJQUFQLEdBQWtCLFNBQUMsUUFBRCxFQUFXLFNBQVg7U0FBeUIsUUFBQSxDQUFTLFFBQVQsRUFBbUIsSUFBbkIsQ0FBc0IsQ0FBQSxDQUFBO0FBQS9DOztBQUNsQixLQUFLLENBQUEsU0FBRSxDQUFBLENBQVAsR0FBa0IsU0FBQyxRQUFELEVBQVcsU0FBWDtTQUF5QixRQUFBLENBQVMsUUFBVCxFQUFtQixJQUFuQixDQUFzQixDQUFBLENBQUE7QUFBL0M7O0FBRWxCLEtBQUssQ0FBQSxTQUFFLENBQUEsT0FBUCxHQUFrQixTQUFDLFFBQUQsRUFBVyxTQUFYO1NBQXlCLFFBQUEsQ0FBUyxRQUFULEVBQW1CLElBQW5CO0FBQXpCOztBQUNsQixLQUFLLENBQUEsU0FBRSxDQUFBLEVBQVAsR0FBa0IsU0FBQyxRQUFELEVBQVcsU0FBWDtTQUF5QixRQUFBLENBQVMsUUFBVCxFQUFtQixJQUFuQjtBQUF6Qjs7OztBRDVDbEIsT0FBTyxDQUFDLEtBQVIsR0FBZ0I7O0FBRWhCLE9BQU8sQ0FBQyxVQUFSLEdBQXFCLFNBQUE7U0FDcEIsS0FBQSxDQUFNLHVCQUFOO0FBRG9COztBQUdyQixPQUFPLENBQUMsT0FBUixHQUFrQixDQUFDLENBQUQsRUFBSSxDQUFKLEVBQU8sQ0FBUCJ9
