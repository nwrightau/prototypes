# Define device type
Framer.Device.customize
	deviceType: Framer.Device.Type.Mobile
	devicePixelRatio: 1
	screenWidth: 375
	screenHeight: 667
	deviceImageWidth: 375
	deviceImageHeight: 667

Utils.CORSproxy = (url) ->

	# Detect local IPv4/IvP6 addresses
	# https://stackoverflow.com/a/11327345
	regexp = /(^127\.)|(^192\.168\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^::1$)|(^[fF][cCdD])/

	if regexp.test(window.location.hostname)
		return "http://#{window.location.host}/_server/proxy/#{url}"
	else
		return url

# Define API request params
apiUrl = "https://staging.api.gmhba.com.au/REST/product/detail"
apiRequestDob = "10/12/2000"
apiRequestLifestage = "S"
apiRequestState = "VIC"
apiRequestRebateTier = 0
apiRequestPaymentFreq = 52

# 52/week
# 26/fn
# 12/month
# 1/year

apiProducts = ''

# Request products from API
xmlhttp = new XMLHttpRequest()   # new HttpRequest instance 

xmlhttp.open("POST", Utils.CORSproxy(apiUrl))

xmlhttp.setRequestHeader("Content-Type", "application/json")

xmlhttp.send(JSON.stringify({"dob":apiRequestDob,"partnerdob":"","lifeStage":apiRequestLifestage,"state":apiRequestState,"rebateTier":apiRequestRebateTier,"paymentFrequency":apiRequestPaymentFreq,"need":"XX","groupDiscount":"DISC-02","brand":"GMHBA","coverType":2,"effectiveFrom":"NOW","hospitalCoverLevel":"high","extrasCoverLevel":"high","abcSettingID":0,"pageCustomContentID":0,"isSalesPortal":false}))

#covertype  2 = combined, 1 = extras only, 0 = hospital only

# Split dollars/cents function
currencySplit = (num) -> 
	amount = num.toString()
	dollars = amount.split('.')[0]
	cents = amount.split('.')[1] || '00'
	dollars = dollars.split('').reverse().join('').replace(/(\d{3}(?!$))/g, '$1,').split('').reverse().join('')
	# Add trailing 0s
	if cents.length <2
		cents = cents+'0'
	return [dollars,cents]

{DynamicLoader} = require "DynamicLoader"

# Add find module
{ƒ,ƒƒ} = require 'findModule'

# Load web font
Utils.loadWebFont("Open Sans")

# Vitality info overlay
flow = new FlowComponent
flow.showNext(Quote, scroll: false)

# Set default amount of products before API Call
defaultProducts = [0,1,2]

# Variables
pageCount = 4
gutter = 10

# Create ScrollComponent for Vertical scrolling
scroll = ScrollComponent.wrap(Quote)
scroll.scrollHorizontal = false
scroll.mouseWheelEnabled = true
Quote.draggable.directionLock = true

# Create PageComponent for the Carousel
pageScroller = new PageComponent
	point: Align.center
	width: product_skeleton.width
	height: product_skeleton.height
	scrollVertical: false
	clip: false
	velocityThreshold: 4
	parent: Product_slider
	y: Slider_nav.y + 30
	x: Align.center
	animationOptions:
		curve: "spring(100,10,0)"
		time: 0.25

pages = []

# Loop to create pages
for index in [0...pageCount]
	page = product_skeleton.copy()
	page.parent = pageScroller.content
	page.x = (pageScroller.width + gutter) * index
	page.y = 0
	page.z = 9
	pages.push(page)
	page.onClick ->
		pageScroller.snapToPage(this)
		
pageScroller.snapToPage(pages[1])

# Navigation
More_cover_btn.onTap ->
	pageScroller.snapToNextPage()
	
Less_cover_btn.onTap ->
	pageScroller.snapToPreviousPage()

products = []

xmlhttp.onreadystatechange = () ->
	if this.readyState == 4 && this.status == 200
		apiResponse = JSON.parse xmlhttp.response
		apiProducts = apiResponse.Products
		apiProductInfo = apiResponse.ProductInfoList
		
		for productItem, index in defaultProducts
			
			if index == 1
				product = Vitality_product_template.copy()
			else
				product = Product_template.copy()
				
			product.parent = pages[index].parent
			product.x = pages[index].x
			product.y = pages[index].y
			product.name = "product"+index
			product.z = 0
			products.push(product)
			
			productServicesList = []
			productLower = []
			
			for layer in product.descendants
				if layer.name == "productText"
					layer.template = 
						"product_name": apiProducts[index].Name
						"product_desc" : apiProducts[index].SellingPoint
						"dollar": currencySplit(apiProducts[index].Rate.combinedNetAmount)[0]
						"cents": currencySplit(apiProducts[index].Rate.combinedNetAmount)[1]
				if layer.name == "SellingPointTxt"
					productServicesList.push(layer)
				if layer.name == "product-lower"	
					productLower.push(layer)
			
			# Render services list
			i = -1
			textHeights = []
			for productService in productServicesList
				i++
				productServices = ''
				for feature in apiProducts[i].Features
					title = apiProductInfo[feature.ProductInfoIndex].Text
					if title == 'Optical'
						productServices += title + ' *\n \n'
					else
						productServices += title + '\n \n'
				productService.text = productServices
				textHeights.push(productService.text.length)

			# Set product lower heights to highest text size
			lowerHeight = Math.max.apply(Math, textHeights) + SellingPointTxt.y * 2

			#for productL in productLower
				#heightDifference = lowerHeight - productL.height
				#productL.height = lowerHeight
				#productL.y = productL.y + heightDifference
			setTimeout (->
				for page in pages
					page.destroy()
			), 2500
			
		# Product 1, Child 2 = Vitality header
		products[1].children[2].onTap ->
			flow.showOverlayCenter(overlay)
		vitalityRevealBtn = products[1].descendants[5]
		vitalityRevealPanel = products[1].descendants[3]
		servicesList = products[1].descendants[1]
		
		vitalityRevealPanel.states =
			inactive: 
				height:0
			active:
				height: vitalityRevealPanel.height
			animationOptions:
				time: 0.45
		vitalityRevealPanel.states.switchInstant "inactive"
		
		servicesList.states =
			inactive:
				y: servicesList.y
			active: 
				y: servicesList.midY + vitalityRevealPanel.height + 20
			animationOptions:
				time: 0.45
		
		vitalityRevealBtn.onTap ->
			vitalityRevealPanel.stateCycle "active","inactive"
			servicesList.stateCycle "active", "inactive"

close_overlay.onTap ->
	flow.showPrevious(Quote, scroll: false)
