# Set up FlowComponent
flow = new FlowComponent
flow.showNext(Login)

# Login page
btn_register.states.inactive =
	grayscale: 100

btn_register.states.active =
	grayscale: 0
	scale: 1.00
	opacity: 1.00

btn_register.animate "inactive",
	time: 1.8
	curve: Spring

btn_register.states.switchInstant "inactive"

btn_register.onTapStart (event, layer) ->
	btn_register.stateSwitch "active"

btn_register.onTapEnd (event, layer) ->
	btn_register.stateSwitch "inactive"	

btn_register.onTap (event, layer) ->
	flow.showNext(Register)
	
btn_back.onTap (event, layer) ->
	flow.showPrevious()
	
btn_back_1.onTap (event, layer) ->
	flow.showPrevious()

btn_reg_continue.onTap (event, layer) ->
	flow.showNext($2_factor)


# 2FA resend

blueColour = "#00A3E0"
greenColour = "#47CC5B"

btn_resend_txt.states.sending = 
	html: btn_resend_txt.html.replace("RE-SEND?", "SENDING...").replace("0, 163, 224", "255, 255, 255")
	options:
		curve: Spring
		time: 0.2

btn_resend_txt.states.sent = 
	html: btn_resend_txt.html.replace("RE-SEND?", "SENT").replace("0, 163, 224", "255, 255, 255")
	options:
		curve: Spring
		time: 0.2

btn_resend_txt.states.resend = 
	html: btn_resend_txt.html.replace("RE-SEND?", "RE-SEND?")
	options:
		curve: Spring
		time: 0.2

btn_resend.states.sending = 
	backgroundColor: blueColour
	options:
		curve: Spring
		time: 0.2
	
btn_resend.states.sent = 
	backgroundColor: greenColour
	html: "<style>[name=btn_resend] > div {border-color:"+greenColour+"!important}</style>"
	options:
		time: 0.01 #  border wierdly animates slower for some reason...
		curve: Bezier.easeInOut
		
btn_resend.onTap (event, layer) ->
	btn_resend_txt.stateCycle "sending", "sent", "resend"
	btn_resend.stateCycle "sending", "sent", "default"

btn_login.states.shelf = 
	y: btn_login.midY + 50
btn_register.states.shelf = 
	y: btn_register.midY + 50
	
input_password.onTap (event, layer) ->
	btn_login.stateCycle "shelf", "default"
	btn_register.stateCycle "shelf", "default"
			
	
	
	
	
	