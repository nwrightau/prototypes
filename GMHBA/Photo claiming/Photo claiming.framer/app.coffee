# Define and set custom device
Framer.Device.customize
	deviceType: Framer.Device.Type.Tablet
	devicePixelRatio: 2
	screenWidth: 750
	screenHeight: 1334
	deviceImage: "http://f.cl.ly/items/001L0v3c1f120t0p2z24/custom.png"
	deviceImageWidth:  750
	deviceImageHeight: 1334

{ CameraInput } = require "CameraInput"

TakePhotoButton = new CameraInput
	width: 200
	height: 200
	opacity: 0
	accept: "image"
	parent: Photo
	callback: (url, type) -> 
		Photo.image = url
