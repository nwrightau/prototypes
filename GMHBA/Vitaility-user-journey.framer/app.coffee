# Define and set custom device
Framer.Device.customize
	deviceType: Framer.Device.Type.Tablet
	devicePixelRatio: 1
	screenWidth: 1440
	screenHeight: 900
	deviceImage: "http://f.cl.ly/items/001L0v3c1f120t0p2z24/custom.png"
	deviceImageWidth: 1440
	deviceImageHeight: 900


# Set up FlowComponent
flow = new FlowComponent
flow.showNext(slide1)

# Switch on click
slide1.onClick ->
	flow.showNext(slide2)

slide2.onClick ->
	flow.showNext(slide3)

slide3.onClick ->
	flow.showNext(slide4)

slide4.onClick ->
	flow.showNext(slide5)
	
